import React, { useState, useEffect, useRef } from "react";
import HttpClient from "./HttpClient.js"
const API_KEY = "gdwjphWDU038gRbOZBeOX0oNWQZEdPGM";
const ENDPOINT = `https://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&q=`;

function Index() {

    const [theSearch, setTheSearch] = useState("");
    const [searched, setSearched] = useState([]);

    const buildUri = (txQuery) =>{
    	return ENDPOINT + txQuery + "&limit=20";
    }

    const search = async () => {
    	let uri = buildUri(theSearch);
    	let response = await HttpClient.get(uri);
    	console.log("here")
    	console.log(response["meta"]["status"])
    	if(response["meta"]["status"]=== 200){
    		setSearched(response["data"]);
    		console.log("here01")
    	console.log(response["data"])
    	console.log(searched.length)
    	}else{
    		alert("Sorry, i can't find out any about your search. Try something else.")
    	}
    }

    const renderThumbnail = (gify, ix) =>{
    	let uriT = "";
    	try{
    		uriT = gify["images"]["original"][0]["url"];
	
    	}catch(error){
    		uriT = gify["images"]["original"]["url"];
    	}

    	return (
    		<div>
    			<img src={uriT} />
    		</div>)
    
    }

    const renderSearch = () => {
        if (searched.length <= 0) {
            return <p>Sorry i cant find out any about your searched, try something else</p>;
        }
        return (
            <div>
            	{ searched.map((gify, i) => renderThumbnail(gify, i))} 
			</div>
        )
    }

    return (
        <div>
			<br /><br />
			<input placeholder="Search your favorite gif" onChange={(evt)=>{setTheSearch(evt.target.value)}} value={theSearch} type="text" />
			<button onClick={()=>{search()}}>Search</button>
			<br />
			{renderSearch()}
		</div>
    )


}

export default Index;