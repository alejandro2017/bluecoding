//let path = require('path');
//const process_ = require('process');
//let who = process_.argv[process_.argv.length - 1];
//who = who.replace("--who=", "");

let modl = {
    "rules": [{
        "test": /\.(js|jsx)$/,
        "use": {
            "loader": "babel-loader",
            "options": { "presets": ['@babel/preset-react'] }
        }
    }]
};
let entry0 = "./src/dev.js";

module.exports = {
    "module": modl,
    "entry": entry0,
    "devServer": { index: "index.html", port: 3001 }
};